# Project Final 

[](url)https://designmuseum.brussels/en/plasticarium-15-essentials-dondolo/

# Partie 1 : L’objet final 

LUCENDO, inspiré par l’œuvre totale « Dondolo » CESARE LEONARDI & FRANCA STAGIvoic est une lampe créée en une seule bande pour obtenir une création singulière. Je me suis amusée à créer différentes courbes pour former un design intéressant dans l’idée de « Less is More ». 
Dans l’optique d’évoquer l’irréel, le rêve, la légèreté, un moment figé dans le temps et dans l’espace. On y retrouve un travail de la courbe qui rend l’objet dynamique et inspirant.

 ![](./images/Capture.JPG)


C’est lors des premières modélisations de la chaise Dondolo que j’ai trouvé intéressant de travailler la courbe différemment et de s’en servir pour créer des utilités différentes.


Une simple courbure, un simple point de vue qui viendrait changer complètement le fondement même de l’objet. Pour donner suite aux différents prototypes réalisés tout au long du cours, j’ai préféré englober complètement la lampe pour accentuer son design dans le but d’apporter une certaine légèreté à l’objet.


En retravaillant cette idée, j’ai donc exercé différents essais, le projet modulable offre plusieurs possibilités d’usages. On retrouve à nouveau un travail sur les courbes mais on obtient aussi des ambiances différentes un jeu de couleurs, d’intensité de transparence et de lumière.

Une vision éclairée sur un design totale et ouverte aux créations et à l’imagination infinie de possibilités 

![](./images/5.JPG)


![](./images/moooo.JPG)


•	Assemblage des deux bords de bandes
Pour pallier aux problèmes concernant le mauvais maintien des bandes, comme on peut le voir sur la photo, j’ai donc crée des rainures en zigzag pour un meilleur maintient 

![](./images/zigzag.JPG)

![](./images/croquis.JPG)


•	La luminosité diffère, quant à elle, selon le type d’ampoules que l’on met, chaude ou froide. Si on utilise du polypropylène, il faut utiliser une ampoule froide pour ne pas chauffer le plastique et donc risquer de le faire fondre.  


•	Courbure de la bande différentes possibilités à explorer selon la taille de la bande, l’épaisseur, l’endroit des rainures leur orientation et leur longueur. Voici néanmoins les deux choix retenus pour ce jury final : 

•	Pour ce dernier essai, j’ai créé grâce à des raccords électriques, permis la modularité des lampes qui seront donc interchangeables avec plusieurs modèles disponibles pour répondre au goût de tous 

•	Pour cette exposition, j’ai créé un support grâce à différentes pièces en métal pour que l’exposition soit la plus complète possible, je l’ai ensuite peint en noir.

![](./images/maintien.JPG)



Des formes délicates et gracieuses, qui donnent l’impression de voler au-dessus de la pièce et qui crée des émotions. 



Exposition au musée du design de Bruxelles

![](./images/musée.JPG)


# Partie 2 : Recherches et prototypes
## Choix du mot


J’ai choisi le mot « inspiration » car c’est celui qui collait le plus selon moi à ma manière de travailler par le fait de plutôt suivre un fil conducteur en m’inspirant de son style mais sans reproduire. M’inspirant par exemple de la courbe, du mouvement mais en essayant quand même d’y mettre ma patte. Plutôt comme un source graphique riche à visualiser. « Phase de la respiration pendant laquelle l'air atmosphérique, riche en oxygène, pénètre dans les poumons. », donc un moment d’air pour sortir sa tête du projet lorsqu’on bloque ou que l’on n’a plus d’idées.






_**Définiton:** Larousse:_  "Inspiration"  

•**Phase de la respiration pendant laquelle l'air atmosphérique, riche en oxygène, pénètre dans les poumons.**


•**Mouvement intérieur**, impulsion qui porte à faire, à suggérer ou à conseiller quelque action: Suivre son inspiration. 


•Enthousiasme, **souffle créateur qui anime l'écrivain**, l'artiste, le chercheur: Chercher l'inspiration. 


•Ce qui est ainsi inspiré: De géniales inspirations.



•Influence exercée sur un auteur, sur une œuvre: Une décoration d'inspiration crétoise. •Influence charismatique de Dieu sur les auteurs des livres saints



===> Comment etre inspiré et avoir une idée cohérente pour ce projet final? Je me suis évidemment demandée quel mot était pour moi intéressant et ensuite dans quel contexte cette inspiration me venait? 


_**La sedia Dondolo di Cesare Leonardi e Franca Stagi**_



 ![](./images/dondolo.JPG)



[](url)(https://www.pinterest.fr/)
 
L’inspiration vient-elle de la couleur, du lieu, de l’ambiance, d’une sensation ?  
En réalité elle est présente dans bien des aspects et dans chaque moment.  Elle peut venir d’une musique simplement grâce aux paroles ou alors nous inspirer quelque chose par l’ambiance dans laquelle elle nous met. 
Peut aussi venir d’illustrations, aquarelles, dessins peintures qui nous entoure.




# Conception d'un objet

*Comme fil conducteur par rapport à mon objet de base et ma lampe designée, j'ai retrouvé les **rayures**.



J’ai eu des difficultés à me décider de quel objet je voulais modéliser car on n’avait pas de réel « interdit » et justement ça m’a un peu perdue sur quelle idée je voulais développer en restant technique, artistique et que qu’il soit réalisable et stable. Je suis d’abord partie sur une idée de boite à masque et ensuite une chaise mais je me suis finalement positionnée sur un lampadaire. Celui-ci serait une déclinaison de ma précédente lampe en ajoutant l’aspect technique qu’il me manquait dans la première.





**Fil conducteur**; il est apparu que celui-ci était la « bande » en une seule pièce et donc j’ai retravaillé cet aspect déjà travaillé sur le projet précédent. Un objet complet réalisé grâce à une seule bande et qui, par sa courbe vient créer une nouvelle forme, un nouvel objet.

Ce que j’ai ressorti de ce projet était le fait que ma première lampe avait atteint une certaine légèreté mais n’était pas assez technique. J’ai donc voulu palier ce manquement sous les conseils de ma précédente correction.



La consigne donnée au cours était de réaliser une lampe qui aurait un certain rapport avec notre objet initialement choisi et sur lequel il n’y aurait aucun ajout supplémentaire pour faire tenir la lampe comme de la colle, du fil etc... 
Pour moi, les éléments de la chaise « dondolo » que je voulais retranscrire sur ma lampe était les rainures qui venait souligner la matière de l’objet et l’unique bande qui vient en une seule courbe créer un objet de manière assez délicate.



J’ai donc créé un prototype en maquette pour voir à quoi cette lampe ressemblerait et quelle courbure je voulais mettre en place pour voir une lampe la plus affinée possible



**_Voici les prototypes et réalisations de ma première lampe:_**

![](./images/esquisse2.JPG)



Après cet essai, je me suis rendu compte que je devais travailler à l'échelle pour une meilleure courbure mais que je devais aussi avant tout travailler par rapport au matériau et créer un design pensé selon le matériau comme un grand couturier qui travaille un vêtement selon le tissu.



![](./images/prot2.JPG)

![](./images/prot1.JPG)







![](./images/1.JPG)
![](./images/2.JPG)
![](./images/3.JPG)

**Ce que je ressors de ces premiers essais:**

- La taille des entailles est trop petite, la feuille de ne tient pas correctement dedans, , j'ai donc prolongé les entailles.

- Retravail du plan de découpe pour l'améliorer selon le matériau qui est plus rigide que celui de l'esquisse.

- Placement des entailles en biais ou droit, ça change le positionnement de la courbe.

- Changement du dessin de la bande en y plaçant un arc.

- Superposition de bandes.










**Fil conducteur**, il est apparu que celui-ci était la « bande » en une seule pièce et donc j’ai retravaillé cet aspect déjà travaillé sur le projet précédent. Un objet complet réalisé grâce à une seule bande et qui, par sa courbe vient créer une nouvelle forme, un nouvel objet.

_Ce que j’ai ressorti de ce projet était le fait que ma première lampe avait atteint une certaine légèreté mais n’était pas assez technique. J’ai donc voulu palier ce manquement sous les conseils de ma précédente correction._ 


















# **CONCEPTION D'UN LUSTRE**



Lampe tombante dans le but de créer une émotion et de pouvoir allier une technique d’assemblage de la bande 
J’ai donc relié cette lampe grâce à une technique assez simple et réalisable Point de couture + rainures (ancienne lampe) mais cette technique n’aurait pas permis de tenir une lampe qui pend. Il a donc fallu ajouter ce point de couture. De plus, cette technique nous permet de lier la bande et par la même occasion de d’assembler l’abat-jour au squelette de la lampe.



# 1. Premiers prototypes


Pour ces premiers essais j’ai utilisé un simple fil fin pour faire office de fil électrique. J’ai réalisé plusieurs essais grâce à une bande plus longue et plus fine que la dernière fois.



*Matériel utilisé*: ![](./images/a.JPG)




![](./images/b.JPG)

![](./images/l.JPG)
![](./images/n.JPG)

**Problèmes rencontrés** : Le fil était trop fin pour maintenir les deux bandes soudées l’une à l’autre. Je pensais donc qu’il serait judicieux de faire plusieurs fentes qui donnerait la possibilité de mieux maintenir ses bandes. Ce fil passerait dans les bandes comme un « point de couture ».
De plus, l’ampoule était bien trop grande pour ce prototype. 

![](./images/c.JPG)

**Solutions apportées** :

Pour ce problème, on peut faire plusieurs trous (3 idéalement) et y insérer le fil. Les bandes seront grâce à ce système, bien fixées les unes aux autres. 

![](./images/point.JPG)


# 2. Découpe des bandes:  



•  La taille des entailles est trop petite, la feuille de ne tient pas correctement dedans, , j'ai donc prolongé les entailles.

•  Retravail du plan de découpe pour l'améliorer selon le matériau qui est plus rigide que celui de l'esquisse.

•  Placement des entailles en biais ou droit, ça change le positionnement de la courbe.

•  Changement du dessin de la bande en y plaçant un arc.

•  Superposition de bandes.






**Note:** Erreur à ne pas faire: Evidemment si ces découpes sont trop proches ou ci elles se croisent, le prototype va se découper. J’ai fait naïvement l’erreur de croiser les deux rainures. Ça à donc évidemment tout découpé et ce fut un échec. Comme on peut le voir dans l'image ci-dessous

![](./images/echecs.JPG)




















  















      


# **_CONCEPTION FINALE_**







_Pour ce lustre, j’ai utilisé dans bandes plus longues et sans rainures pour ne pas en faire trop car j’avais déjà 3 lampes différentes.
La longueur de la bande, la forme de cette bande et le placement des entailles est très important. Des rainures trop proches peuvent ne pas rester assemblées ou ne créeront pas la courbure voulue.
Une fois que le modèle est fixé, on peut le réaliser sur Autocad ou un autre logiciel de dessin Ensuite on découpera ses bandes au Lasersaure comme expliqué plus en détail dans le « module 4 ». _





## 1.Couleurs et matières de la bande :




**Lequel utiliser ?**



Pour cette lampe, j'ai voulu avant tout travailler par rapport au matériau et créer un design pensé selon le matériau comme un grand couturier qui travaille un vêtement à partir du tissu.




Polypropylène ep :0.2.
 Je pense qu’il est aussi possible de réaliser cet objet avec d’autres matériaux tel qu’une feuille de métal. Je possédais ici des machines disponibles au FabLab, mais j’aimerais vraiment voir ce que cette lampe donnerait en métal.


_Qu'est ce que le Polypropylène?_


> Le chimiste italien Giulio Natta et le chimiste allemand Karl Ziegler obtinrent, en mars 1954, un polypropylène à structure géométrique cristalline régulière . Natta emploie le terme « isotactique » pour décrire ce polymère. Les chaînes d'un polymère isotactique peuvent se rapprocher les unes des autres pour former un solide ordonné. 
Plus tard, il mettra au point des catalyseurs stéréospécifiques permettant l'obtention systématique de tels polymères. >

![](./images/F.JPG)



Attention de ne pas réaliser cette lampe en papier ou papier cartonné car elle pourrait chauffer et bruler le papier. De plus, le papier cartonné se plie trop facilement et cela perturbe la courbe désirée à la base.
De plus, on peut assembler les matières, les couleurs et jouer avec les découpes. 

![](./images/D.JPG)


**Note** : Il faut faire attention de mettre une ampoule et un ruban proportionnels. Ca peut paraitre logique, mais c'est à mentionner: si le ruban est trop petit, trop large ou que l’ampoule prend le dessus sur le design ça ne donnera pas l’effet voulu.

![](./images/ruban.JPG)


L’idée de cette lampe est aussi que tout un chacun puisse s’imaginer la sienne. 
Qu’on puisse changer les couleurs ainsi que la forme. Elle est voulue modulable. 
Et se forme avec une simple bande. Je l’imagine dans plein de formes, de couleurs et des matériaux différents. De plus, on peut changer le nombre d’ampoules, la taille des bandes, ajouter des rainures, changer le type d’éclairage, etc...

J’aimerais, si j’en ai l’occasion, essayer cette lampe avec un polypropylène coloré. Cela pourrait donner une vraie ambiance à une pièce. De plus j’aimerais voir comment la légère luminosité de la lampe pourrait diffuser le couleur dans un espace.



## 2.MATERIEL UTILISE : 



- Polypropylène : pour les bandes que l’on va modéliser.

- Fil : On va pouvoir choisir la couleur (j’ai pris un fil transparent pour l’esthétique), la section de 0.75mm² (qui était suffisante pour supporter l’intensité consommée par ses ampoules), l’épaisseur du fil m’a ici permise de soutenir la lampe seule, et donc ne pas créer des « points de couture »).De plus, grâce au deuxième isolant, je peux me permettre d’utiliser ce fil comme « armature » sans aucun autre soutient car il est suffisamment fort pour supporter son poids.

- Un socle avec le nombre de trous dont vous avez besoin (j’ai modélisé le mien sur l’imprimante 3D Prusa du Fablab). J’ai fait 3 fentes, pour 3 ampoules.

- Un soquet par lampe 

- Un arrêt par lampe : (serre-câble) 

- 3 Ampoules 
- Un raccord qui va permettre de faire le lien électrique sur le haut de la lampe. (Sucre)




![](./images/materiel_bon.JPG)







## Socle:  

-Il est imprimé en 3D et c’est cet élément qui vient cacher les fils, parties plutôt techniques de la lampe. Il faudrait au meilleur des cas créer un rebord aux trous du socle pour qu’il paraisse plus fini.

-Mettre le nombre de trous que l’on a besoin pour chaque lampe.

- Attention à bien vérifier le diamètre du trou avant impression. J’ai moi-même imprimé un socle avec des trous trop étroit pour le passage du fil que j’avais choisi. Je conseille donc, de connaitre le diamètre de son fil avant de réaliser le socle. Si, vous avez comme moi fait l’erreur, pour pouvait délicatement passer un tournevis d’un plus grand diamètre dedans. (Pour s’en servir de test. Car cette technique ne rend pas un produit fini). 

-Maintien des fils au niveau de l’intérieur du socle grâce à des colsons.

-J’ai pour ma version de du pré-jury, placé du Tesa sur l’intérieur du socle pour une question de protection

![](./images/socle.JPG)




## 3.ETAPES DE REALISATION : 





- Premièrement on enlève l’isolant qui fait office de support sur le fil. On vient ensuite le « dénuder » pour pouvoir faire le raccord électrique

==>L’épaisseur de fil correspond à la consommation de l’ampoule, cette section sera ensuite couverte par un isolant (double isolant dans notre cas). Le second isolant sert de support à la lampe. Il peut être utilisé sans isolant externe car il est lui-même assez protégé.
Pour le fil, il y a 2 phases différentes à raccorder donc 3 fils de chaque côté pour qu’il soit alimenté avec la même tension car pour ma lampe je veux la même intensité lumineuse dans chacune de mes ampoules.

![](./images/FIL.JPG)



![](./images/y.JPG)



![](./images/raccc.JPG)




**Note** : Le diamètre du culot doit correspondre au soquet choisi. Si le culot est trop grand, l’ampoule ne s’insérera pas.

![](./images/amp.JPG)




Le raccord électrique dit le « sucre » vient se raccorder à la fin de la structure et idéalement dans le socle pour cacher cette pièce.

==> 3 Ampoules alimentées en 220V , branchée en parallèle.





- Venir fixer l'ampoule 

![](./images/rac.JPG)



- Couper les fils de la longueur souhaitée. Je suis venue, après raccords faits, positionner des colsons juste au-dessus du socle pour que la longueur ne bouge pas. 

![](./images/t.JPG)

- Placer les bandes modélisées.

- Inserer les fils dans le socle

**Note** : Attention à toujours bien tenir tous les fils lorsqu’on ajuste les lampes sur le socle. C’est un moment clé car en doit tenir tous les fils et les imbriquer dans le socle en même temps. Je conseille de les entrés une à unes pour ne pas faire la même erreur que moi…

![](./images/r.JPG)

- Venir placer le socle pour cacher les raccords.

- Placer l'interrupteur de l'autre coté du raccord (dans le socle). Pour cette étape il faut d'abord dénuder les fils comme nous l'avons fait précedemment. On raccorde les lampes d'un coté et l'interrupteur de l'autre.

![](./images/sucre.JPG)

- Fixer les colsons pour bloquer les lampes dans le socle 


En fin de chaine la lampe se branche idéalement comme une lampe traditionnelle, grâce aux raccords électriques dans la maison. Ici, étant donné que je devais déplacer l’objet j’ai raccordé la lampe à une prise possédant un interrupteur et pour pouvoir l’allumer sur place.
Dans l’habitat, la lampe serait normalement reliée à la terre, et donc protégé par un fusible. Ce qui n’est pas le cas ici 





_Laisse place à l’imagination…_




Pourquoi 3 ampoules sur cette lampe ? Cet assemblage permet de se rendre compte que plusieurs plis sont possibles et nous permet de se projeter (comment aurais-je, moi, plié cette bande ? En aurais-je plusieurs, en quelle couleur l’aurais-je fais ?) 
De plus, cela m’a permis de jouer sur les couleurs et donc sur la luminosité et les ambiances que cette lampe procurerait.

Cette lampe peut être déclinée sous plusieurs formes et plusieurs couleurs. Elle peut être déclinée sous plein de formes différentes et d’adapter à chaque intérieur. Elle créerait à chaque fois une nouvelle ambiance singulière et propre à elle-même.





# **_RESULTAT FINAL_ **



![](./images/f1.JPG)

![](./images/f2.JPG)


![](./images/f3.JPG)



![](./images/g.jpg) 







