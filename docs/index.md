## Présentation 





Bonjour à tous, je m'appelle **Molly Scarfalloto** et je fais mon master en architecture à L'Université libre de Bruxelles. 
Je viens de Mons où j'y ai fais mon bachelier. Je suis d'origine italienne et pasionnée de peinture, de voyages et de sport.


J'ai choisi l'option design dans le but d'élaborer des modèles virtuels en 3D et de pouvoir les réaliser dans le réel. J'adore réaliser des maquettes et je pense que la découverte de nouvelles machines du FabLab serait un atout, des nouvelles compétences à acquérir et j'ai plus que hate de découvrir ça. J'ai toujours eu un peu de mal pour soigner ma maquettes et les réaliser poprement, c'est pourquoi j'ai pensé que ce cours pourrais me donner une nouvelle approche et m'aider dans mes réalisations 3D, qui jusqu'ici étaient assez baclées. Pourtant, les maquettes sont pour moi un réel outil pour percevoir et donner envie d'aller plus loin lors d'une présentation de projet.


Le parcours de Louis de Fabribeckers est un vrai modèle pour moi et me fascine énormenent par ses ambitions et ses créations.




![](images/avatar-photo.jpg)


- 


##Quelques peintures et dessins

![](images/pb.JPG)


![](images/db.JPG)


- **PORTFOLIO PROJETS** 
[](url)https://wetransfer.com/downloads/e386fe337808e69d624ba93e0ddf144a20201102002614/15371b3c766d8bdde147437984a0859b20201102002614/9ed96e



## Objet choisi:


L'objet que j'ai choisi est le fauteuil à bascule Conçu en 1968, dondolo en italien. Il est une sorte d’oeuvre totale imaginée par le duo italien Cesare Leonardi et Franca Stagi.
Par sa finesse et son élégance cet objet est basé quasi entierement sur une courbe simple, son allure résolument futuriste pour son temps m'a donné envie de le choisir.
L’emploi de la fibre de verre permet de mouler l’objet d’une seule pièce. D’une perfection sculpturale, l’ensemble adopte la forme d’une courbe qui traduit et sert la fonction de l’objet


![](images/123235643_656089078603735_5499671135145168012_n.jpg) 


