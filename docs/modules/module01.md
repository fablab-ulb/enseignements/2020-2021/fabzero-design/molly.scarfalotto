  # 1. DONDOLO CESARE LEONARDI, FRANCESCA STAGI






Conçu en 1968, le fauteuil à bascule "dondolo" en italien est une sorte d’oeuvre totale imaginée par le duo italien Cesare Leonardi et Franca Stagi. L’allure résolument futuriste de cette chaise illustre parfaitement l’influence de la conquête spatiale sur la conception des formes durant les années 60. La Dondolo sort des usines en même temps que l’homme pose pour la première fois le pied sur la lune. L’emploi de la fibre de verre permet de mouler l’objet d’une seule pièce. D’une perfection sculpturale, l’ensemble adopte la forme d’une courbe qui traduit et sert la fonction de l’objet le tout dans un esprit d’élégante abstraction.
Les cannelures - ces sillons en creux dans la matière - permettent d’obtenir un très bon rapport entre la résistance et le poids, tout en induisant par un jeu de pleins et de creux, d’ombre et de lumière - un extraordinaire sentiment de mouvement et de fluidité. L’assise et le dossier semblent flotter dans l’air, ce qui n’est pas sans évoquer l’apesanteur des voyages spatiaux. 
Disponible en trois couleurs [blanc, gris et bleu], la Dondolo a été l’une des pièces phares présenté à Italy : The new Domestic Landscape [MoMA, 1972], exposition qui a célébré le design Italien.



L’italien CESARE LEONARDI [1935-] s’intéresse au design au cours de sa deuxième année à l’école d’architecture de Florence. Dès ses premières années d’études, les éléments clefs de sa pratique  l’expriment : il cherche à optimiser les performances de la matière combinées avec la synthèse de la structure et de la forme. Très tôt dans sa carrière, il s’associe à FRANCA STAGI [1937-] pour parvenir à ces objectifs.
Ensemble, ils conçoivent en 1961 le fauteuil Ruban CL9, constituée d’une bande continue en fibre de verre [GRP] soutenue par un tube métallique et dont le ADAM possède également un exemplaire.


![](../images/sample-photo.jpg)






J'ai choisi cet objet car son aspect immaculé, plastic m'a tout de suite tapé dans l'oeil. J'ai apprécié l'unique courbe travaillée qui venait créer un objet sans ajout exterieur, juste une bande recourbée sur elle-même . De plus, je voulais choisir comme objet une chaise pour l'aspect pratique que je trouvais plus agréable à travailler. 




## **GitLab**

Pour le cours de design, il nous est demandé de retranscrire notre méthodologie de travail sur GitLab dans le but de transmettre nos compétences aquises et partager les connaisances entre étudiants.

GitLab est un site de partage qu'il faut bien maitriser pour pouvoir l'utiliser correctement. 




## Installation


Tout d'abord il faut installer Atom et GitHub Desktop. Cela va nous permettre de creer un compte su
 https://gitlab.com/users/sign_up  . Ensuite il faudra aller sur notre pofil étudiant pour créer notre User ID.

 Nous pouvons istaller un editeur de texte tel que [](https://atom.io/) ou [](https://atom.io/packages/markdown-preview-enhanced).

 Personellement, j'ai d'abord écris mes textes sur Word pour les fautes d'orthographe et je suis venue coller mes textes et ajouter mes images, url, vidéos par la suite.


![](../images/git2.JPG)

## Informations utiles pour utiliser le site

- Ne pas oublier de compresser les images, vidéos etc. Cette étape est très importante car le site peut très vite être saturé. Cette étape peut=etre réalisée simplement sur Photoshop ou alors en allant sur un site de compression d'images tel que [](url)https://compressjpg.online/fr/ 
Pour réaliser cette étape sur Photoshop il faut:

Dimension initiale de l'image à importer : 1365x1365 = 5,3 Mo
Utilisation de photoshop pour compresser l'image en format pour le Web
Fichier -> Exporation -> Enregistrer pour le web -> Ajustez les paramètres -> Enregistrer
Fichier compressé : 400 x 400 = 126 Ko

- Pour ajouter une nouvelle image il faut aller dans douments puis > Upload new file> et ensuite inserer cette image dans l'editeur de texte à l'endroit voulu. Attention il ne faut pas oublier d'ajouter " ![] (image/(nom de l'image) ou bien l'image n'apparait pas.



- Il existe certains **codes** qui permettent différentes actions tel que
: ##TEXTE ## : Permet de créer un titre.
: ** TEXTE ** : Permet de mettre le texte en gras.
: __ TEXTE __ : Permet de mettre le texte ne italique.

- Une fois les changements réalisés, cliquez sur "Commit Changes" en bas de la page pour sauvagarder ce qui a été fait.


  

## **Fusion 360**

Dans l'après-midi 
Ce programme est assez intuitif pour les personnes ayant déja utilisé des programmes de modelisation tel que Sketchup ou Archicad. Ce programme est assez intuitif mais certaines étapes sont à connaitre pour bien comprendre les tenants et aboutissant de la création pour Fusion360.
Pour les étudiants qui utilisent un abonnement utilisateur unique, accédez au logiciel gratuit de CAO/FAO 3D Fusion 360 pour les étudiants.


**VIDEOS DU COURS**
Pour pouvoir comprendre et travailler sur le programme j'ai utilisé ces deux vidéos explicatives du cours
- [](url)https://web.microsoftstream.com/video/66a278b5-48ec-4198-bed6-0bd0a863efb6
- [](url)https://web.microsoftstream.com/video/c9e3743c-6094-442a-b774-78d94bdfc386


**TUTOS**
Ensuite j'ai eu besoin de quelques tutos pour réaliser différentes actions, en voici les liens :


- [](url)https://www.youtube.com/watch?v=W9lVf2FEFuc
- [](url)https://www.youtube.com/watch?v=bh13sZN4TPc&t=618s 
- [](url)https://www.youtube.com/watch?v=c0jFd1uHF8I 







