# 2. Conception assistée par ordinateur



En deuxième semaine d'option de design, nous devions modéliser notre objet et l'imprimer si grâce à l'imprimer avec dans l'optique de recréer la chaise dondolo vue au musée, il a fallu recréer cet objet sur Fusion 360. 

![](../images/sample-pic-2.jpeg) 




Nous pouvions néanmoins exporter notre modèle 3D sur un autre programme pour réaliser certaines actions qui nous seraient plus compliquées voir impossible sur Fusion360.
Bienvenue sur l'interface de Fusion 360. Voici les outils de la barre d'outil principale. Plutot intuitif d'utilisation , voici les outils qu l'on a à l'ouverture du programme.

![](../images/barre_outil_2.png)
 

## Premier prototype


**Etapes** 

J'ai d'abord retracé l'objet intuitivement pour voir si je savais bien utiliser le programme. 
J'ai donc premièrement créé une esquisse. 
Voici la barre d'outils qui apparait quand on clique sur "esquisse", il faut aussi choisir le plan quand lequel on travaille comme on peut le voir ici. Très proche des outils autocad, j'ai personnellement eu plus de facilité à créer des nouvelles esquisses que lorsqu'il fallait commencer à modéliser en 3D.

![](../images/barre_outil.png)


![](../images/esquisse.JPG)




J'ai ensuite retracé les contours grâce à l'outil **Spline**, après cela il a fallu créer un décalage au niveau de l'esquisse pour que celle-ci aie une épaisseur et donc pour pourvoir en créer une forme. Une fois le contour créé, j'ai utilisé l'outil **décaler** puis relié le tracé pour obtenir une forme fermée et pouvoir l'extruder par la suite.

![](../images/esq.JPG) 

J’ai ensuite utilisé l’outil **extruder** pour y créer de la matière et ainsi commencer à modéliser en 3D. 
![](../images/extru.JPG)


Création de la coque, en sélectionnant la face voulue et en sélectionnant l’outil **Coque**

![](../images/coqueee.JPG)



Création des **congés**, qui viennent arrondir des angles pour créer une forme pour réaliste. Pour cela il faut sélectionner l’outil congé et ensuite choisir l'arrete en question. Après cela, on peut choisir l'angle plus ou moins aigu selon les attentes.Si on vaut des bords coupés mais pas arrondis on doit utiliser l'outil **Chanfrein**


![](../images/conges.JPG)


 Cela m'a fait me rendre compte que on pouvait retravailler l'objet de manière à inverser l'assise et le siège pour placer la personne plus droite et ainsi recréer une nouvelle manière de s'assoir sur cette chaise a bascule. Cette chaise longue s'est différenciée de celle à l'initial.

Attention : Ne pas oublier d’enregistrer vos fichiers ! Alors oui ça peut sembler bête mais ça m’est arrivé… Donc toujours bien prendre la peine de vérifier si tout est enregistrer car sinon c’est une grosse perte de temps.

## Deuxième prototype


Premièrement, j'ai placé l'image de l'objet à l'échelle dans le but de reproduire efficacement et rigoureusement la forme de la chaise. J'ai fais: insertion, puis choi la face sur laquelle je placerais mon image. Puis j'ai créé une esquisse et retracé la forme grace à l'outil Spline, comme pour le premier prototype mais à l'echelle cette fois ci. Un fois que l'image est retracée et que je n'en ai plus eu besoin, j'ai placé mon curseur en bas de l'écran juste avant l'image. Ainsi, je pouvais la faire réaparaitre quand cela était utile.Il est aussi possible de la selectionner et de faire supprimer image.

![](../images/image_insert.JPG)


  


**ECHEC**
Pour ce deuxième prototype j'ai voulu recréer les rainures interieures que la chaise d'origine possède. J'ai essayé par plusieurs moyens différents qui n'ont pas aboutis. Le premier essai était basé sur un prototype plein et où je viendrais "creuser" les rainures par la suite mais cela n'a pas fonctionné car la ligne de la rainure ne suivait jamais correctement la courbe (en tout cas je n'y suis pas parvenue). 
J'ai essuite essayé de créer des petites faces à l'interieur de la coque pour ensuite travailler avec l'outil "follow me" Mais encore une fois, les faces ne se mettaient pas sur le bon plan et une fois mis sur le bon plan, quelque chose bloquait et ne voulait pas suvre la forme de la courbe existante.


![](../images/eche.JPG )  






En posant la question à Tejhay qui avait la meme chaise que moi, j'ai appris qu'il avait créé plusiseurs "corps" en puis il avait assembler ces différents corps crées pour construire la face place. Les rainures étaient donc plusieurs corps assemblés.

J'ai donc suivi sa méthode:

Création d'une premiere esquisse, puis décalage, extrusion, coque interieure,copier-coller.

![](../images/bonesqui.JPG)


Maintenant le problème était de venir assembler tout cela. J'ai opté pour la méthode de venir décaler une très petite surface du premier corps, pour ensuite l'extruder sur tous les corps suivants en donc ainsi venir les relier les uns aux autres.

![](../images/corps.JPG)

![](../images/ass.JPG )


Finalament, je suis venue y ajouter un rendu en cliquant sur rendu, ajouter matière, plasic blanc. Ensuite, il faut faire glisser le materiau sur les faces, ou selectionner tout l'objet et faire glisser le materiau quand il y a beaucoup de faces.


![](../images/rendu.JPG)



