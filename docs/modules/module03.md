# 3. Impression 3D



**Qu’est-ce qu’une impression 3D ?**


 L'impression 3D ou fabrication additive regroupe les procédés de fabrication de pièces en volume par ajout ou agglomération de matière, par empilement de couches successives. L'impression 3D permet de réaliser un objet réel : un concepteur dessine l'objet 3D grâce à un outil de conception assistée par ordinateur. Le fichier 3D obtenu est traité par un logiciel spécifique qui organise le découpage en tranches des différentes couches nécessaires à la réalisation de la pièce. Le découpage est envoyé à l'imprimante 3D qui dépose ou solidifie la matière couche par couche jusqu'à obtenir la pièce finale. Le principe reste proche de celui d'une imprimante 2D classique à cette grande différence près : c'est l'empilement des couches qui crée le volume.


**Original Prusa I3 MK3S**


C’est une imprimante imprimée par des imprimantes ce qui est complétement incroyable car il a donc fallu créer l’imprimante une seule fois et était ensuite capable de se recréer à l’infini sans aide supplémentaire.

Elle se déplace sur les axes X, Y et Z

Pour télécharger l’application il suffit de se rendre sur le site et le télécharger, aucun abonnement n’est demandé et c'est gratuit.
[](url)https://www.prusa3d.fr/

Quelques avantages :  Qualité d’impression exceptionnelle 
Solidité 
Communauté enthousiaste et coopérative 
Autocalibrage 
Détection des chocs 
Mise en pause et reprise d’impression 
Ponts et surplombs de qualité 
Prise en main facile du logiciel de découpe Slic3r PE 
Réactivité et compétence du service client Prusa

![](../images/123409878_397553434987575_3258565993309767867_n.jpg)

# Utilisation 

Voici l'écran d'accueil quand on ouvre l'outil Prusa :

![](../images/m.JPG)
 On voit donc le plateau de l'imprimante où l'on va voir notre objet s'imprimer. Le plateau est aimanté et on peut donc l'enlever. C'est un élément souple qui nous permet lorsque notre objet est finalisé de le plier un peu pour décoller l'objet sans l'endomager. Il faut nettoyer le plateau à l'acetone avant et surtout après utilisation pour ne pas laisser de reste de filament sur le plateau.


#Premier prototype 

J'ai d'abord importé mon objet 3D en version STL mais ça fonctionne aussi en version OBJ, cela veut donc dire que pour l’exporter sur Fusion il faut l’exporter dans ce format. 
![](../images/importer.JPG) 

Ensuite, il faut sélectionner le mode expert permettant d’avoir accès à plus de paramètres qui nous seront utiles par la suite. Il y a des petites choses à savoir pour faire un bon paramétrage de PrusaSlicer, par exemple c'est important de savoir que la buse de l'imprimante 3D peut aller jusqu'à 0.4 mm d'épaisseur de trait. Cela l'est d'autant plus dans mon projet puisque les parois sont très fines et fragiles à petite échelle. Il y a aura donc une échelle minimale à ne pas dépasser.
Devant chaque paramétrage apparait un hexagone coloré. S’il est vert, ça veut dire que le réglage est simple, si il est orange, il s'agit d'un réglage avancé, et si il est rouge, c'est un réglage d'expert.
Il faut également savoir que si le paramètre devient orange et n'est plus noir comme les autres, c'est qu'il à été modifié. 

Les outils sur la gauche permettent d'agrandir, de déplacer, de tourner l'objet sur sa face qui sera positionnée sur le plateau.

![](../images/position.JPG)


Ensuite il faut faire les réglages en fonction de notre objet
![](../images/reglages.JPG) 

•	Couche et périmètre : Pour la hauteur de couche, il faut toujours mettre 0.2mm et ne pas toucher à celle de la première couche. Pour les parois verticales, le périmètre doit être à 3 par défaut pour le minimum afin d'avoir une certaine solidité de l'objet. Après libre à nous de modifier le type de remplissage intérieur, cela dépendra de la résistance de l'objet attendu, mais ce sera pour l'explication suivante ça. Pour le reste, on ne touche à rien sauf si cela est utile dans le cas d'un objet qui en nécessiterait. Sinon on peut laisser par défaut.
•	Remplissage : La densité de remplissage doit idéalement toujours être comprise entre 10 et 15% si la résistance mécanique de l'objet est faible. On peut monter jusqu'à 35% si vraiment cela est nécessaire mais pas au-dessus, ce serait du gaspillage de matière. D'ailleurs les formes de remplissage les plus efficaces sont les formes gyroïdes et en nid d'abeille.
•	Jupe et bordure : La bordure c'est pour garder la pièce collée au plateau pendant l'impression, elle permet donc d'avoir une plus grande emprise au sol, il s'agit d'une légère couche supplémentaire sur le plateau, autour de notre objet, qui maintient la pièce. Les valeurs invariables correspondent aux dimensions de la buse. La jupe définit le périmètre d’impression sur le plateau Il y a par défaut une jupe de programmée sur l'objet pour que celui-ci soit bien stable sur le plateau, mais si la pièce que l'on imprime en 3D est top haute ou doit être stabilisée, on va demander à créer un contour supplémentaire pour stabiliser l'objet lors de son impression et éviter qu'il ne tombe ou s'imprime mal.
•	Supports : on peut générer des supports supplémentaires pour maintenant une partie de l'objet qui serait en porte-à-faux ou aurait besoin de support pour ne pas tomber. Il suffit de cocher la case. Les supports permettent de pallier les ponts. Parfois, certains supports ne sont pas indispensables, cette   case   permet   donc de faire « une sélection» des supports nécessaires. De préférence, il vaut mieux les générer uniquement sur le plateau de l'imprimante : cela permet une économie de matière et de temps surtout. Plus on utilise de la matière, plus ce sera long et couteux.
 Autres réglages : il vaut mieux laisser par défaut et jouer éventuellement avec si on possède un cas spécial, si on pas besoin d'y toucher. Au niveau du filament, il faudra bien paramétrer la machine en fonction du filament qu'on insère dedans. Si on a un doute sur la quantité de fil qu'il reste et qu'on pense que la machine n'ira pas au bout de la pièce, on peut soit mettre en pause et changer la bobine vide en cours d'impression, soit par sécurité la changer avant de commencer, mais il ne faut surtout pas que la bobine soit vide en cours d'impression et qu'elle chauffe à vide. Ca pourrait l'abimer. Sélectionner le bon type de filament pour que les paramètres se remplissent automatiquement

Il faut aussi prévoir la taille de l'objet final avant de lancer l'impression ! Le plateau n'est pas très grand, il faudra donc bien gérer les échelles de son projet. La taille du plateau est à peu près de 20 x 20 cm et 22cm de hauteur.
Quand tout cela est finalisé, on appui sur la touche TAB avec les deux flèches inversées, et cela met à jour la visualisation 3D finale.


![](../images/bon.JPG)

![](../images/espacement.JPG)

Quand tous les réglages sont en ordre, on exporte le G Code. On vient l'importer ensuite sur la carte SD qui s'insère dans la machine directement. Pour activer l'impression il suffit de choisir le bon dossier et de le selectionner. 
Cette impression était rapide car c'est un petit objet. On peut voir sur cette vidéo, l'effet satisfaisant de l'imprimante.

 ![](../vidéos/-1604263986__1_.mp4)


#Deuxième prototype :

Pour celui-ci j'ai voulu changer la couleur du filament.
Il a donc fallu aller dans le menu puis sélectionner **unload filament** le fil va chauffer et donc le détacher, il faudra donc l'enlever. Ensuite, on met le filament dans la couleur qu'on souhaite et on selectionne **load filament** la couleur précédente va sortir petit à petit pour donner place à la nouvelle couleur. Après on aura un message qui va s'afficher pour savoir si la couleur est la bonne. La bobine est ainsi changée.

![](../images/pro2.JPG)

Cette impression quant à elle, était plus longue, environ 6h.


![](../images/proto2.JPG) 


![](../images/prototype2.JPG)





Il faudra finalement venir enlever tous les supports, attention à ne pas abimer le prototype avec cette étape. J'ai personnellement utilisé une pince à épiler et un scalpel pour être la plus précise possible. Cette étape prend du temps, à ne pas sous-estimer.










