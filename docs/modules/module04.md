# 4. Découpe assistée par ordinateur


Création d’un nouvel objet lié au Dondolo de Cesare Francesco (mon objet choisi).

Pour ce dernier cours en présentiel, nous avons eu l’occasion de découvrir le fonctionnement de la découpeuse laser.


# Laser Cuter 


La découpe laser est un procédé de fabrication qui consiste à découper la matière grâce à une grande quantité d’énergie générée par un laser et concentrée sur une très faible surface. Cette machine est plutôt simple d’utilisation. 
Mais est à prendre avec beaucoup de précautions car une mauvaise utilisation peut être dangereuse et peut causer des dégâts.

 Il est donc important de connaitre les consignes de sécurité avant de l’utiliser seul.



**Utilisation**



Premièrement il faut allumer l’échappée d’air (si ça fait beaucoup de bruit c’est que c’est allumé). Il faut aussi allumer le refroidisseur et les deux autres derrières. 
Le bouton rouge est utile en cas d’arrêt d’urgence de la machine. Il faut ensuite tourner le bouton dans l’autre sens quand on veut pouvoir le réutiliser. 
Et finalement, ne pas oublier de fermer le capot de l’imprimante. Si un de ces points n’est pas respecté, l’impression ne se mettra pas en route car le programme détecte un danger possible et ne s’enclenchera donc pas.
Sous notre matériau il y a un élément appelé « nid d’abeilles » qui est là pour rehausser l’objet et créer une aération sous les matériaux.



 
![](../images/laser2.JPG)![](../images/laser3.JPG)




# Réalisation d’une lampe

La consigne donnée au cours était de réaliser une lampe qui aurait un certain rapport avec notre objet initialement choisi et sur lequel il n’y aurait aucun ajout supplémentaire pour faire tenir la lampe comme de la colle, du fil etc... 
Pour moi, les éléments de la chaise « dondolo » que je voulais retranscrire sur ma lampe était les rainures qui venait souligner la matière de l’objet et l’unique bande qui vient en une seule courbe créer un objet de manière assez délicate.



J’ai donc créé un prototype en maquette pour voir à quoi cette lampe ressemblerait et quelle courbure je voulais mettre en place pour voir une lampe la plus affinée possible


![](../images/esquisse2.JPG)



J’ai ensuite retracé le prototype sur Autocad (version étudiante) pour avoir des épaisseurs bien définies. 


# Première découpe

*Etapes de cette impression:*


Pour l’épaisseur de la bute, celle-ci fait 3mm, il faut donc réaliser ses conceptions en tenant compte de cet élément et donc ajuster la hauteur en conséquence. On peut regler cet élément grâce à des petits cales disponibles sur place, j'ai ici dû déviser la bute, mettre mon élément à découper en dessous, y placer le cale de 15mm et puis reviser à la bonne hauteur. On doit y faire très attention quand on passe d'une épaisseur à une autre mais si le matèriau à découper a la meme épaisseur, ça ne sert à rien de modifier à chaque fois.  



Pour cette première impression, il a fallu exporter le document d'Autocad (*PDF*) vers Illustrator pour pouvoir l'exporter (*SVG*). 


Une fois le docmument sur une clef USB, on ouvre le programme. Ensuite ouvrir le programme Inksckape pour pouvoir dégrouper le modèle 2D (*ungroup*). 


On ouvre le programme Driveboardapp.

Notre dessin doit s'afficher avec les différentes couleurs que l'on peut venir sélctionner.
Ne pas oublier d'appuyer sur la petite maison en bas de la page qui permet de revenir à zéro. Les petites flèches à côté du bouton "run" permettent de voir la dimension que prendra l'impression et donc si on a bien placé la feuille. L'origine est le petit curseur rouge sur le programme. On peut le déplacer sur l'ecran et il viendra donc se déplacer dans l'imprimante, c'est utile si on veut savoir comment/ où se placer.

![](../images/staut.JPG)

Chosir l'epaisseur des *rainures* et des *découpes* (rouge et noir dans ce cas) on peut avoir plus que deux couleurs et ainsi avoir différentes profondeurs. 
Quand le statut est vert (tout est fonctionnel) et que les reglages sont faits, on peut donc presser le bouton *RUN* pour activer l'impression. En haut à gauche on à le temps d'impression qui s'affiche sur l'écran.


![](../images/laser1.JPG)




J'ai par cet essai, compris que vu que le materiau du prototype et de cet essai étaient assez différents au niveau de leur rigidité, il faudrait adapter de modèle car il ne crée plus la courbure comme avant. Le prototype se tord et ne rend pas le résultat voulu. 
De plus, les entailles étant trop ressérées en leur centre, le plastique crée sur le bord une bande doite. La courbe est aussi trop petite par rapport au résultat voulu. 


![](../images/prot2.JPG)

![](../images/prot1.JPG)















# Deuxième découpe



Après cet essai, je me suis rendu compte que je devais travailler à l'échelle pour une meilleure courbure mais que je devais aussi avant tout travailler par rapport au matériau et créer un design pensé selon le matériau comme un grand couturier qui travaille un vêtement selon le tissu.

![](../images/20.JPG)



- La taille des entailles est trop petite, la feuille de ne tient pas correctement dedans, , j'ai donc prolongé les entailles.

- Retravail du plan de découpe pour l'améliorer selon le matériau qui est plus rigide que celui de l'esquisse.

- Placement des entailles en biais ou droit, ça change le positionnement de la courbe.

- Changement du dessin de la bande en y plaçant un arc.

- Superposition de bandes.


**Echec**

J’ai pour cet essai voulu créer des rainures plus soutenues et j’ai donc coupé dans le matériau au lieu de créer une simple rainure un peu trop discrète à mon gout. Donc des découpes au lieu des entailles du deuxième prototype. 
Evidemment si ces découpes sont trop proches ou ci elles se croisent, le prototype va se découper. J’ai fait naïvement l’erreur de croiser les deux rainures. Ça à donc évidemment tout découpé et ce fut un échec.

![](../images/echecs.JPG)









**Résultat**




D'impressions en impressions... Voici le résultat! 

 Pour finir, j’ai finalement superposé deux prototypes, ce qui crée une hauteur et un certain mouvement à la structure. 






![](../images/1.JPG)
![](../images/2.JPG)
![](../images/3.JPG)
![](../images/124578769_3794124037298514_7673395079946055365_n.jpg)



